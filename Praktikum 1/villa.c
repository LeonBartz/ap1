#include <stdio.h>

int main() {
    int x1;
    int x2;
    float x3;
    float x4;
    int etagen = 3;
    int preis = 150;

    printf("Bitte gib x1 ein:");
    scanf("%d", &x1);

    printf("Bitte gib x2 ein:");
    scanf("%d", &x2);

    x3 = x1 / 5;
    x4 = x2 / 2;

    float ganzeFlaeche = x1 * x2;
    float nullflaeche = (x1 - 2 * x3) * x4;

    float etagenflaeche = ganzeFlaeche - nullflaeche;

    float hausflaeche = etagenflaeche * etagen;

    printf("\nDie Flaeche einer Etage Betraegt %f\n Die Flaeche de gesammten Hauses ist %f\n", etagenflaeche, hausflaeche);

    float kosten = etagenflaeche * preis;

    printf("\nEin Weg um das Haus würde %f Euro kosten\n", kosten);

    return 0;
}
