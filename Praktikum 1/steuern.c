//
// Created by leonb on 23.10.2019.
//
#include <stdio.h>
int main(){

    float netto;
    float brutto;
    float mehrwert;

    printf("Programm zur Berechnung eines Bruttobetrages\n");
    printf("\nBitte geben Sie den Nettobetrag in EUR ein:");
    scanf("%f", &netto);

    mehrwert = netto * 0.19;
    brutto = netto - netto * 0.19;
    printf("Nettobetrag                  = %.2f EUR\n", netto);
    printf("Mehrwertsteuersatz 19.00 %%   = %.2f EUR\n", mehrwert);
    printf("Bruttobetrag                 = %.2f EUR\n", brutto);
    return 0;
}
