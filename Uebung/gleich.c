//
// Created by Leon Bartz on 25.11.19.
//
#include <stdio.h>

int gleich(int a, int b){
    return (int) a == b;
}

int main(){
    int a = 0, b = 0;
    int same;
    scanf("%i %i", &a, &b);
    same = gleich(a, b);
    printf("%d\n", same);
}

