//
// Created by Leon Bartz on 25.11.19.
//
#include <stdio.h>

int add(int a, int b, int *c){
    *c = a + b;
    return 0;
}

int main(){
    int c = 0;
    add(1, 2, &c);
    printf("%i\n", c);
    return 0;
}
