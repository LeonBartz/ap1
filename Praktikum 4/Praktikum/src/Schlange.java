public class Schlange {

    private int nextFree;
    private Taxi[] arr;

    public Schlange(int nextFree) {
        this.nextFree = nextFree;
        arr = new Taxi[5];
    }

    public void dequeue(){
        if(is_empty())return;
        arr[0] = null;

        for (int i = 0; i < arr.length - 1; i++){
            arr[i] = arr[i+1];
            arr[i+1] = null;
        }
        nextFree--;
    }

    public void enqueue(Taxi taxi){
        if(is_full()){
            System.out.println("Fehler: Das Taxi: " + taxi.getnummer() + ", " + taxi.getnamefahrer() + ", " + taxi.getkennzeichen() + " kann nicht einfahren! Der Taxistand ist picke apcke voll!");
            return;
        }
        arr[nextFree] = taxi;
        System.out.println("Das Taxi: " + taxi.getnummer() + ", " + taxi.getnamefahrer() + ", " + taxi.getkennzeichen() + " fährt auf Platz" + nextFree + 1);
        nextFree++;
    }

    public boolean is_empty(){
        return nextFree == 0;
    }

    public boolean is_full(){
        return nextFree == arr.length;
    }

    public void ausgeben(){
        //System.out.println(java.util.Arrays.toString(arr));
        for (Taxi taxi: arr) {
            if(taxi != null){
                System.out.print(taxi.getnummer() + " ");
            }else{
                System.out.print("frei ");
            }
        }
        System.out.println();
    }

    public void clear(){
        System.out.println("Alle 5 Plätze sind leer.");
        this.nextFree = 0;
        arr = new Taxi[5];
    }

}
