public class App {
    public static void main(String args[]) {
        Taxi a = new Taxi("Frank Victor", "BN - FV 300", 1);
        Taxi b = new Taxi("Angela Merkel", "B - DE 001", 2);
        Taxi c = new Taxi("James Bond", "BN - JB 007", 3);
        Taxi d = new Taxi("Manuel Neuer", "M - MN 001", 4);
        Taxi e = new Taxi("Angelique Kerber", "BN - AK 111", 5);
        Taxi f = new Taxi("Boris Becker", "M - BB 4911", 6);

        Schlange taxistand = new Schlange(0); // Nächster freier Platz
        // ist im Index 0
        System.out.println("Ausgangssituation");
        taxistand.clear(); // leert den Taxistand
        taxistand.ausgeben(); // zeigt den Taxistand (siehe Ausgabe)

        System.out.println("1. Situation");
        taxistand.clear(); // leert den Taxistand
        taxistand.enqueue(a);
        taxistand.enqueue(b);
        taxistand.enqueue(c);
        taxistand.enqueue(d);
        taxistand.enqueue(e);
        taxistand.enqueue(f);
        taxistand.ausgeben();
    }
}