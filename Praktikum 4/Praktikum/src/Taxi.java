/**
 * Ein Taxi ist ein Objekt, das aus dem Fahrernamen, dem Kennzeichen und
 * einer eindeutigen Nummer besteht.
 */
public class Taxi {
    private String namefahrer;
    private String kennzeichen;
    private int nummer;
    /**
     * Der Konstruktor legt ein Taxi Objekt an und speichert darin den
     * Fahrernamen, das Kennzeichen und die Taxinummer.
     */
    public Taxi(String namefahrer, String kennzeichen, int nummer) {
        this.namefahrer = namefahrer;
        this.kennzeichen = kennzeichen;
        this.nummer = nummer;
    }

    /**
     * Diese Methoden verwenden wir zur Ausgabe der Meldungen in der Klasse
     * Schlange.
     */
    public String getnamefahrer() {
        return namefahrer;
    }

    public String getkennzeichen() {
        return kennzeichen;
    }

    public int getnummer() {
        return nummer;
    }
}
