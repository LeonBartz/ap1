import java.util.Scanner;

public class Nr2 {

    private static Scanner scan = new Scanner(System.in);

    public static void main(String Args[]) {
        int x, y;
        System.out.print("Bitte geben Sie x ein: ");
        x = scan.nextInt();
        System.out.print("Bitte geben Sie y ein: ");
        y = scan.nextInt();

        try {
            System.out.println("Ackermann(" + x + "," + y + "): " + ackermann(x,y));
        } catch (Exception e){
            System.out.println(e.toString());
        }

    }

    public static long ackermann(long n, long m) {
        if (n == 0 )
            return m + 1;
        else if (m == 0 )
            return ackermann(n - 1, 1 );
        else
            return ackermann(n - 1, ackermann(n,m - 1) );

    }



}
