import java.util.Locale;
import java.util.Scanner;
import static java.lang.Math.pow;
import static java.util.Locale.*;

public class Nr4 {
    public static void main(String[] args) {

        setDefault(new Locale("de", "DE"));

        System.out.println("Programm zur Berechnung der Potenz.");

        Scanner scanner = new Scanner(System.in);
        System.out.print("Bitte geben Sie x ein: ");

        int x = scanner.nextInt();

        System.out.print("Bitte geben Sie y ein:  ");

        int y = scanner.nextInt();

        System.out.printf(GERMANY, "Sie Potenz von x hoch y ist: %.2f.\n", + pow(x, y));

    }
}
