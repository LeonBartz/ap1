public class Queue {

    private int nextFree;
    private int[] arr;
    private int length;

    public Queue(int length){
        arr = new int[length];
        nextFree = 0;
        this.length = length;
    }

    public void dequeue(){
        if(is_empty())return;
        arr[0] = 0;

        for (int i = 0; i < arr.length - 1; i++){
            arr[i] = arr[i+1];
            arr[i+1] = 0;
        }
        nextFree--;
    }

    public void enqueue(int value){
        if(is_full())return;
        arr[nextFree] = value;
        nextFree++;
    }

    public boolean is_empty(){
        return nextFree == 0;
    }

    public boolean is_full(){
        return nextFree == length;
    }

    public void print_queue(){
        System.out.println(java.util.Arrays.toString(arr));
    }
}
