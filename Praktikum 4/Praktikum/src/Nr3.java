import java.util.Arrays;

public class Nr3 {
    public static void main(String[] args) {
        System.out.println("Program Running");
        int[] numbers = { 2, 4, 10, 12, 23};//new int[100];

        /*for(int i = 0; i < numbers.length; i++){
            numbers[i] = i;
            System.out.println(i + " " + numbers[i]);
        }*/

        int res = findNumberInArray(numbers, 1, 0, numbers.length);
        if(res == -1){
            System.out.println("Kein Ergebniss gefunden!");
        }else{
            System.out.println(res + " " + numbers[res]);
        }

    }

    public static int findNumberInArray(int[] array, int number, int first, int last){

        if(last < first){
            return -1;
        }

        int half = (first + last) / 2;

        if(array[half] == number){
            return half;
        }

        if(array[half] > number){
            return findNumberInArray(array, number, first, half -1 );
        }else{
            return findNumberInArray(array, number, half + 1, last);
        }

    }
}
