//
// Created by Leon Bartz on 27.11.19.
//
#include "stdio.h"

int printArr(int *arr, int len){
    int i;
    for(i = 0; i < len; i++){
        printf("%i: %i\n", i, arr[i]);
    }
    return 0;
}

int main(){
    int ZahlenAnzahl = 5;
    int Zahlen[ZahlenAnzahl];
    Zahlen[0] = 7;
    Zahlen[1] = 1;
    Zahlen[2] = 6;
    Zahlen[3] = 3;
    Zahlen[4] = 4;

    printArr(Zahlen, ZahlenAnzahl);
    printf("------------------------\n");
    int i, j, tmp;
    for (i = 1; i < ZahlenAnzahl; ++i) {
        for (j = 0; j < ZahlenAnzahl - i ; j++)
        {
            if (Zahlen[j] > Zahlen[j+1])
            {
                tmp = Zahlen[j];
                Zahlen[j] = Zahlen[j+1];
                Zahlen[j+1] = tmp;
            }
            printArr(Zahlen, ZahlenAnzahl);
            printf("------------------------\n");
        }
    }

    printArr(Zahlen, ZahlenAnzahl);

    return 0;
}