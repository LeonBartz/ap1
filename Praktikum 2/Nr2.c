//
// Created by Leon Bartz on 27.11.19.
//
#include "stdio.h"

int main(){
    int grenze;

    printf("Bitte gib deine Zahl ein: ");
    scanf("%i", &grenze);
    if(grenze <= 1 || grenze > 1000){
        printf("Die Zahl liegt nicht im Zahlenbereich!\n Probier es mit einer Zahl zwischen 2 und 1000.\n");
        return 0;
    }

    int Zahlen[grenze];
    int j;
    for (j = 0; j < grenze; j++){
        Zahlen[j] = 1;
    }
    Zahlen[0] = 0;
    Zahlen[1] = 0;

    int i;
    int m;
    for (i = 2; i < grenze; i++){

        for(m = 2; m*i < grenze; m++){

            Zahlen[i*m] = 0;

        }
    }

    for(i = 0; i < grenze; i++){
        if(Zahlen[i] == 1){
            printf("Primzahl: %i\n", i);
        }
    }

    return 0;
}