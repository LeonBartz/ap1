//
// Created by Leon Bartz on 25.11.19.
//
#include <stdio.h>
int main() {
    int wuerfel = 5;
    int seiten = 6;
    int zahlen[wuerfel];
    int counter[seiten];
    int l;
    for(l = 0; l < wuerfel; l++){
        zahlen[l] = 0;
    }

    int m;
    for(m = 0; m < wuerfel; m++){
        counter[m] = 0;
    }

    int i;
    for (i = 0; i < wuerfel; i++) {
        printf("Bitte gib Zahl Nr. %d ein: ", i);
        scanf("%d", &zahlen[i]);
        counter[zahlen[i]-1] = counter[zahlen[i]] + 1;
    }
    int j = 0;
    int 3er = 0;
    int 2er = 0;
    for (j = 0; j < seiten; j++) {
        printf("%i: %i\n", j + 1, counter[j]);
        switch (counter[j]){
            case 5: printf("Grand"); break;
            case 4: printf("Poker"); break;
            case 3: 3er = 1; break;
            case 2: 2er = 1; break;
        }
    }
    if(3er && 2er){
        printf("Full House");
    }
    return 0;
}
