//
// Created by Leon Bartz on 27.11.19.
//
#include "stdio.h"

int main(){
    int NotenAnzahl = 5;
    int Noten[NotenAnzahl];
    int KlausurenAnzahl = 0;
    int Durchgefallen = 0;
    int DurchschnittAlle = 0;
    int i;
    for (i = 0; i < NotenAnzahl; i++) {
        printf("Bitte gib die Anzahl der Arbeiten mit der Note %i ein: \n", i+1);
        scanf("%i", &Noten[i]);
    }

    printf("Noten: | Anzahl:\n");
    int j;
    for (j = 0; j < NotenAnzahl; ++j) {
        printf("%i | %i\n", j, Noten[j]);
        KlausurenAnzahl = KlausurenAnzahl + Noten[j];
        DurchschnittAlle = DurchschnittAlle + j*Noten[j];
    }
    printf("Gesammt | %i\n", KlausurenAnzahl);

    printf("Durchfallquote: %.02f\n", (float) Noten[4]/KlausurenAnzahl);
    printf("Durchschnitt: %.02f\n", (float) DurchschnittAlle/KlausurenAnzahl + 1);

    return 0;
}