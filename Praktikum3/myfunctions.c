//
// Created by Leon Bartz on 10.12.19.
//

#include <stdio.h>
#include <string.h>

int laengeZeichenkette(const char str[]){
    return (unsigned) strlen(str);
}

int sucheZeichen(char name[], char buchstabe){

    int counter = 0;
    int i;
    int laenge;

    laenge = laengeZeichenkette(name);

    for (i = 0; i < laenge; ++i) {
        if(name[i] == buchstabe){
            counter = counter +1;
        }
    }
    return counter;

}
