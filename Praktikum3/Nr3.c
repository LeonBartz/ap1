//
// Created by Leon Bartz on 10.12.19.
//
#include <stdio.h>
#include <string.h>

struct Artikel{
    int anzahl;
    int isEmpty;
    char name[20];
};

void artikelErstellen(struct Artikel artikel[]){
    char name[10];
    int counter = 0;
    int leerenGefunden = 0;
    int leererArtikel;
    int anzahl = 0;

    printf("Bitte gib den Namen des Artikels ein: ");
    fflush(stdin);
    scanf("%[^\n]s", name);

    while(counter < 100){
        if(!strcmp(artikel[counter].name, name)){
            printf("Es gibt bereits einen Artikel mit dieser Bezeichnung.\n");
            printf("Wie viele Exemplare sollen eingefügt werde ?");
            fflush(stdin);
            scanf("%i", &anzahl);
            artikel[counter].anzahl = artikel[counter].anzahl + anzahl;
            return;
        }else if(leerenGefunden == 0 && artikel[counter].isEmpty == 1){
            leerenGefunden = 1;
            leererArtikel = counter;
        }
        counter = counter + 1;
    }

    if(leerenGefunden == 0){
        printf("Der Speicher ist voll!\n");
        return;
    }

    artikel[leererArtikel].isEmpty = 0;
    strcpy(artikel[leererArtikel].name, name);

    printf("Artikel erstellt.\n");

    printf("Wie viele Exemplare sollen eingefügt werde ?");
    fflush(stdin);
    scanf("%i", &anzahl);
    artikel[leererArtikel].anzahl = anzahl;

    return;
}

void artikelEntnehmen(struct Artikel artikel[]){

    char name[10];
    int counter = 0;
    int anzahl = 0;

    printf("Bitte gib den Namen des Artikels ein.\n");
    fflush(stdin);
    scanf("%[^\n]s", name);

    while (counter < 100){

        if(!strcmp(artikel[counter].name, name)){

            printf("Artikel gefunden.\nBitte gib die Anzahl an Elementen an die Entnommen werden sollen.\n");
            fflush(stdin);
            scanf("%i", &anzahl);

            if(artikel[counter].anzahl - anzahl < 0){
                printf("Das Ergebnis ist nicht Moeglich.\n");
            }else{
                artikel[counter].anzahl = artikel[counter].anzahl - anzahl;
                printf("Der neue Stand beträgt: %i\n", artikel[counter].anzahl);
            }

            return;
        }

        counter = counter + 1;
    }

    printf("Artikel nicht gefunden!\n");

    return;
}
void eintragSuchen(struct Artikel artikel[]){
    char name[10];
    int counter = 0;

    printf("Bitte gib den Namen des Artikel ein.\n");
    fflush(stdin);
    scanf("%[^\n]s", name);

    while(counter < 100){
        if(!strcmp(artikel[counter].name, name)){
            printf("Der Artikel hat noch %i Elemente\n", artikel[counter].anzahl);
            return;
        }
        counter = counter + 1;
    }

    printf("Der Artikel wurde nicht gefunden\n");
    return;
}
void tabelleAusgeben(struct Artikel artikel[]){
    int counter = 0;
    while(counter < 100){
        if(artikel[counter].isEmpty == 0){
            printf("%s | %i |\n", artikel[counter].name, artikel[counter].anzahl);
        }
        counter = counter + 1;
    }
    return;
}

void menu(struct Artikel artikel[]){
    int eingabe = 0;
    printf("Bitte Waelen Sie eine Aktion:\n[1]: Artikel hinzufegen.\n[2]: Artikel entnehmen.\n[3]: Eintrag suchen.\n[4]: Tabelle ausgeben.\n");
    fflush(stdin);
    scanf("%i", &eingabe);

    switch (eingabe){
        case 1: artikelErstellen(artikel); menu(artikel); break;
        case 2: artikelEntnehmen(artikel); menu(artikel); break;
        case 3: eintragSuchen(artikel); menu(artikel); break;
        case 4: tabelleAusgeben(artikel); menu(artikel); break;
        default: printf("Ungueltige eingaben!\n"); menu(artikel); break;
    }

    return;
}

int main(){
    int counter = 0;
    struct Artikel artikel[100];

    while(counter < 100){
        artikel[counter].isEmpty = 1;
        artikel[counter].anzahl = 0;
        strcpy(artikel[counter].name, "");
        counter = counter + 1;
    }

    menu(artikel);

    return 0;

}