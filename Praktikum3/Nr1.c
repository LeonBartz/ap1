//
// Created by Leon Bartz on 02.12.19.
//

#include <stdio.h>
#include <string.h>

void sucheZeichen(char name[]){
    char buchstabe = ' ';
    int counter = 0;
    int i;
    int laenge;

    laenge = laengeZeichenkette(name);
    printf("Bitte gib den Buchstaben ein:\n");
    fflush(stdin);
    scanf("%c", &buchstabe);

    for (i = 0; i < laenge; ++i) {
        if(name[i] == buchstabe){
            counter = counter +1;
        }
    }
    printf("Der Buchstabe %c kommt %i mal vor.\n", buchstabe, counter);
    return;
}

int laengeZeichenkette(const char str[]){
    return (unsigned) strlen(str);
}

void menu(const char name[]){
    char janein = ' ';
    printf("Moechtest du nach einem buchstaben in %s suchen (j / n)?\n", name);
    fflush(stdin);
    scanf("%c", &janein);

    switch (janein){
        case 'j': sucheZeichen(name); menu(name); break;
        case 'n': printf("Auf Wiedersehen\n"); return;
    }
}

int main(){
    char name[10];
    printf("Bitte gib den Namen ein: \n");
    fflush(stdin);
    scanf("%[^\n]s", &name);

    menu(name);
    return 0;
}